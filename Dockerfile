FROM node:10.17-alpine

COPY . .
RUN sh -c 'yarn global add serve && yarn && yarn build'

RUN adduser -D myuser
USER myuser
CMD serve -l $PORT -s build
