import React from 'react';
import logo from './ale.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a href="https://alediaferia.com" target="_blank"><img src={logo} className="App-logo" alt="logo" /></a>
        <a
          className="App-link"
          href="https://twitter.com/alediaferia"
          target="_blank"
        >
          Follow me <FontAwesomeIcon icon={faTwitter}/>
        </a>
      </header>
    </div>
  );
}

export default App;
