# Continuous Deployment

This is a sample project to demonstrate continuous deployment using Heroku, Gitlab and Docker (buildah).

## Credits

You can find me on [Twitter](https://twitter.com/alediaferia) where I post about software engineering and startups.
